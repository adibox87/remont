@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

        </div>
    </div>
{{--    <div class="row">--}}
{{--        <div class="ml-auto">--}}
{{--            <a href="{{route('apartments.destroy', $apartment)}}" class="btn btn-outline-danger">Удалить объект</a>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="row">
        <div class="col-md-12 text-center">
                <h1>{{$apartment->name}}</h1>
            <div class="text-truncate">{{$apartment->address}}</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 mt-4">
            <div class="card">
                <div class="card-header">
                    Расходы по категориям
                </div>
                <div class="card-body">
                    Тут будет круговая диаграмма, когда-то
{{--                    @include('apartments.tasks_breakdown')--}}
                </div>
            </div>
        </div>
        <div class="col-md-8 mt-4">
            <div class="card">
                <div class="card-header">
                    Добавление расхода
                </div>
                <div class="card-body">
                    @include('payments.create')
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 mt-4">
            <div class="card">
                <div class="card-header">
                    Участники проекта
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            @if($apartment->owner_id == auth()->id())<th scope="col">Удаление</th>@endif
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($apartment->users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    @if($apartment->owner_id == auth()->id())
                                    <td>
                                        <form method="GET" action="{{ route('participation.destroy') }}">
                                                <input type="hidden" name="apartment_id" value="{{$apartment->id}}">
                                                <input type="hidden" value="{{$user->id}}" name="id">
                                                <button class="btn btn-block btn-secondary">Удалить</button>
                                        </form>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if($apartment->owner_id == auth()->id())
                <div class="card-footer">
                    <form method="GET" action="{{ route('participation.store') }}">
                        <div class="input-group my-2">

                                <input type="hidden" name="apartment_id" value="{{$apartment->id}}">
                                <input type="text" class="form-control" placeholder="Email" name="email">
                            <div class="input-group-append">
                                <button class="btn btn-block btn-success">Добавить</button>
                            </div>
                        </div>
                    </form>
                </div>
                @endif
            </div>
        </div>

        <div class="col-md-8 mt-4">
            <div class="card">
                <div class="card-header">
                    Задачи проекта

                    <!-- Button trigger modal -->
                    <button type="button" class="btn-xs btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                        Добавить задачу
                    </button>

                    <!-- Modal -->
                    @include('tasks.create')

                </div>
                <div class="card-body">
                    @foreach($apartment->tasks as $task)

                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <h4>
                                    {{$task->name}}
                                </h4>

                                <div class="progress">
                                    @php($money = $task->payments->sum('payment')/$task->budget*100)
                                    <div class="progress-bar @if($money > 90) bg-danger @endif" role="progressbar" style="width: {{$money}}%" aria-valuenow="{{100/150*100}}" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <p class="card-text text-secondary mt-1">Использовано бюджета {{$task->payments->sum('payment')}}/{{$task->budget}}</p>




                                <a href="{{route('tasks.show', $task)}}" class="btn btn-primary">Подробнее</a>

                            </li>
                        </ul>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>
<script>
</script>
@endsection
