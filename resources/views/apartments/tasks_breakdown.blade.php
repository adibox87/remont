<div class="card mb-3">
    <div class="card-header">
        Category breakdown
    </div>
    <div class="card-body">
        <canvas id="categoryBreakdown"></canvas>
    </div>
</div>

@push('html-body-bottom')
    <script>
        window.addEventListener('DOMContentLoaded', function () {

            var categoryBreakdownData = {
                labels: {!! $tasks->pluck('name')->toJson() !!},
                datasets: [
                    {
                        backgroundColor: {!!
                            $tasks->map(function (\App\Tasks $task) {
                                return $task->color;
                            })->toJson()
                        !!},
                        borderWidth: 0,
                        data: {!!
                            $tasks->map(function (\App\Tasks $task) {
                                return $apartment->getScoreForTask($task);
                            })->toJson()
                        !!}
                    }
                ]
            };

            var categoryBreakdownOptions = {
                cutoutPercentage: 85,
                legend: {position: 'right', padding: 5, labels: {pointStyle: 'circle', usePointStyle: true}}
            };

            new Chart($('#categoryBreakdown'), {
                type: 'pie',
                data: categoryBreakdownData,
                options: categoryBreakdownOptions
            });
        })
    </script>
@endpush
