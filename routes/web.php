<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('apartments', 'ApartmentController');
Route::resource('participation', 'ParticipationController');
Route::resource('tasks', 'TasksController');
Route::resource('payments', 'PaymentsController');

Route::get('/participate', 'ParticipationController@store')->name('participation.store');
Route::get('/tasks/create','TasksController@new')->name('tasks.new');

Route::get('/disparticipate', 'ParticipationController@destroy')->name('participation.destroy');
