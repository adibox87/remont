@extends('layouts.app')

@section('content')
    <div class="container">
        {{ Form::open(['role' => 'form', 'id' => 'apartments-form', 'route' => 'apartments.store', 'method' => 'post']) }}
        @include('apartments._form')
        {{ Form::close() }}
    </div>
@endsection
