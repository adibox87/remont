<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Http\Middleware\Participant;
use App\Payments;
use App\Tasks;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\ApartmentStoreRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class ApartmentController extends Controller
{



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('apartments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApartmentStoreRequest $request)
    {
        $apartment = Apartment::create($request->validated());
        return redirect(route('apartments.show', compact('apartment')));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function show(Apartment $apartment)
    {
        if ($apartment->owner_id == Auth::id() || $apartment->users()->exists(Auth::id()))
        {
            $users = User::all();
            $tasks = Tasks::where('apartment_id', '=', $apartment->id);
            $payments = Payments::all();
            return view('apartments.show', compact('apartment','payments', 'tasks' ,'users'));
        } else return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function edit(Apartment $apartment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Apartment $apartment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Apartment  $apartment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Apartment $apartment)
    {
        //
    }
}
