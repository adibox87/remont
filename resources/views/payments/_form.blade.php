
        <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {{ Form::label('name', 'Назначение расхода') }}
                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('payment', 'Сумма') }}
                    {{ Form::number('payment', null, ['class' => 'form-control']) }}
                </div>
                <div class="form-group">
                    {{ Form::label('tasks_id', 'Задача') }}
                    {{ Form::select('tasks_id', $apartment->tasks->pluck('name', 'id')->all() , null, ['class' => 'form-control'], ['placeholder' => 'Pick a task...'])}}
                </div>
            </div>

            <div class="form-group col-md-6">
                {{ Form::label('description', 'Описание') }}
                {{ Form::textarea('description', null, ['class' => 'form-control', 'rows' => 8]) }}
            </div>


            <div class="col-md-2">
                {{  Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}
            </div>

        </div>
