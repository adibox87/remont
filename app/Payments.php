<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $fillable = [
      'name',
      'payment',
      'description',
      'tasks_id'
    ];

    public function task()
    {
        return $this->belongsTo(Tasks::class);
    }
}
