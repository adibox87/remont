@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">

        </div>
    </div>
{{--    <div class="row">--}}
{{--        <div class="ml-auto">--}}
{{--            <a href="{{route('apartments.destroy', $apartment)}}" class="btn btn-outline-danger">Удалить объект</a>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="row">
        <div class="col-md-12 text-center">
                <h1>{{$apartment->name}}</h1>
            <div class="text-truncate">{{$apartment->address}}</div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Расходы по категориям
                </div>
                <div class="card-body">
                    Тут будет круговая диаграмма, когда-то
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Добавление расхода
                </div>
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    Участники проекта
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Удаление</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @foreach($apartment->users as $user)
                                <tr>
                                    <td>{{$user->name}}</td>
                                    <td>
                                        <form method="GET" action="{{ route('participation.destroy') }}">
                                                <input type="hidden" name="apartment_id" value="{{$apartment->id}}">
                                                <input type="hidden" value="{{$user->id}}" name="id">
                                                <button class="btn btn-block btn-secondary">Удалить</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tr>
                        </tbody>
                    </table>

                </div>
                <div class="card-footer">
                    <form method="GET" action="{{ route('participation.store') }}">
                        <div class="input-group my-2">

                                <input type="hidden" name="apartment_id" value="{{$apartment->id}}">
                                <input type="text" class="form-control" placeholder="Email" name="email">
                            <div class="input-group-append">
                                <button class="btn btn-block btn-success">Добавить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    Задачи проекта
                </div>
                <div class="card-body">
                    @foreach($apartment->tasks as $task)
                        <a href="{{route('tasks.show', $task)}}">{{$task->name}}</a>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
