<?php

use App\Enums\UserRole;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
                [
                    'name' => 'Admin',
                    'email' => 'admin@example.com',
                    'email_verified_at' => NULL,
                    'password' => '$2y$10$KwH6C7yAcpgq2VYXDuhjj.VIn0.lV/ph6OxyMJx6Vla.S1sGnxXoK', // secret
                    'remember_token' => NULL,
                    'created_at' => '2018-10-01 18:39:19',
                    'updated_at' => '2018-10-01 18:39:19',
                ],
        ]

        );

        factory(App\User::class, 200)->create();






    }
}
