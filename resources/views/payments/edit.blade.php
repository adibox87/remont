@extends('layouts._app')

@section('content')
    <div class="container">
        {{ Form::model($apartment,['role' => 'form', 'id' => 'apartmentss-form', 'route' => ['apartments.update',$apartment], 'method' => 'put']) }}
        @include('apartments._form')
        {{ Form::close() }}
    </div>
@endsection
