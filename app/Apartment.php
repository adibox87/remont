<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Apartment extends Model
{
    protected $fillable = [
        'name',
        'address',
        'owner_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'participations', 'apartment_id', 'user_id')
            ->withTimestamps();
    }

    public function tasks()
    {
        return $this->hasMany(Tasks::class);
    }

    public function payments()
    {
        return $this->hasManyThrough(Payments::class, Tasks::class);
    }

    public function getScoreForTask(Tasks $task)
    {
        return $this->payments()->where('task_id', $task->id)->sum('payment');
    }
}
