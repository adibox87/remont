@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="ml-auto">
            <a href="{{route('apartments.create')}}" class="btn btn-outline-primary">Добавить объект</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if(auth()->user()->apartments->count() > 0)
            <h2>Мои объекты</h2>
            <div class="mt-4 card-columns">
                @foreach(auth()->user()->apartments as $apartment)
                    @include('apartments._card')
                @endforeach
            </div>
            @else
                <a href="{{route('apartments.create')}}"><h2>Добавьте свой первый объект</h2></a>
            @endif
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-12">
            @if(auth()->user()->participations->count() > 0)
                <h2>Обекты доступные мне</h2>
                <div class="mt-4 card-columns">
                    @foreach(auth()->user()->participations as $apartment)
                        @include('apartments._card')
                    @endforeach
                </div>
            @endif
        </div>
    </div>
</div>
@endsection
