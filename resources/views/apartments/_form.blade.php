<div class="row">
    <div class="col-md-8 mt-2">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    {{ Form::label('name', 'Название объекта') }}
                    {{ Form::text('name', null, ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::hidden('owner_id', Auth::id()) }}
                </div>
                <div class="form-group">
                    {{ Form::label('address', 'Адрес') }}
                    {{ Form::textarea('address', null, ['class' => 'form-control']) }}
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4 mt-2">
        <div class="card">
            <div class="card-header">Дополнительная информация</div>
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h6>После создания записи вы будуте перенаправлены на главную страницу</h6>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                {{  Form::submit('Сохранить', ['class' => 'btn btn-primary btn-block']) }}
            </div>
        </div>
    </div>
</div>
