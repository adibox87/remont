    <div class="card text-decoration-none">
        <div class="card-header h4">
            {{$apartment->name}}
        </div>
        <div class="card-body">
            <p class="card-text">
                {{$apartment->address}}
            </p>
            <a href="{{route('apartments.show', $apartment)}}" class="btn btn-primary">Подробнее</a>
        </div>
    </div>

