
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Добавление задачи</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <div class="form-group">
                        {{ Form::label('name', 'Название задачи') }}
                        {{ Form::text('name', null, ['class' => 'form-control']) }}
                    </div>

                    {{Form::hidden('apartment_id', $apartment->id)}}

                    <div class="form-group">
                        {{ Form::label('budget', 'Бюджет') }}
                        {{ Form::number('budget', null, ['class' => 'form-control']) }}
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{  Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}
            </div>
        </div>
    </div>
</div>
