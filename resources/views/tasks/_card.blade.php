<a href="{{route('apartments.show', $apartment)}}">
    <div class="card text-decoration-none">
        <div class="card-header h4">
            {{$apartment->name}}
        </div>
        <div class="card-body">
            {{$apartment->address}}
        </div>
    </div>
</a>

