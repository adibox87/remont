<?php

namespace App\Http\Controllers;

use App\Apartment;
use App\Http\Middleware\Participant;
use App\User;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Echo_;

class ParticipationController extends Controller
{

    public function __construct()
    {
        $this->middleware(Participant::class);
    }

    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $apartment = Apartment::find($request->apartment_id);

        if ($user = User::where('email', '=', $request->email)->first()) {
            $apartment->users()->attach($user->id);
            $apartment->save();
            return back();
        }
        else return back();
    }

    public function destroy(Request $request)
    {
        $apartment = Apartment::find($request->apartment_id);
        $apartment->users()->detach($request->id);
        $apartment->save();
        return back();
    }
}
